<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "{{%giftcard}}".
 *
 * @property int $id
 * @property string|null $email
 * @property float|null $balance
 * @property int|null $created_at
 * @property int|null $updated_at
 */
class Giftcard extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%giftcard}}';
    }

    public function behaviors(){
        return [
            TimestampBehavior::class,
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['email','balance','redemptionCode'],'required'],
            [['balance'], 'number'],
            [['created_at', 'updated_at'], 'integer'],
            [['email'], 'string', 'max' => 250],
            [['redemptionCode'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'email' => 'Email',
            'balance' => 'Balance',
            'redemptionCode' => 'Redemption Code',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * {@inheritdoc}
     * @return \app\models\query\GiftcardQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\query\GiftcardQuery(get_called_class());
    }
}

<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%giftcard}}`.
 */
class m210428_181211_create_giftcard_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%giftcard}}', [
            'id' => $this->primaryKey(),
            'email' => $this->string(250),
            'balance' => $this->float(10,2),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%giftcard}}');
    }
}

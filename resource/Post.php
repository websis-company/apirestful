<?php 
namespace app\resource;
/**
 * 
 */
class Post extends \app\models\Post
{
	public function fields(){
        return ['id','title','body'];
    }

    public function extraFields(){
    	return ['created_at','updated_at','comments'];
    }

    /**
     * Gets query for [[Comments]].
     *
     * @return \yii\db\ActiveQuery|\app\models\query\CommentQuery
     */
    public function getComments()
    {
        return $this->hasMany(Comment::class, ['post_id' => 'id']);
    }
}

?>
<?php 
namespace app\resource;
/**
 * 
 */
class Comment extends \app\models\Comment
{
	public function fields(){
        return ['id','title','body','post_id'];
    }

    public function extraFields(){
    	return ['created_at','updated_at','post'];
    }

    /**
     * Gets query for [[Post]].
     *
     * @return \yii\db\ActiveQuery|\app\models\query\PostQuery
     */
    public function getPost()
    {
        return $this->hasOne(Post::class, ['id' => 'post_id']);
    }

}
<?php 
namespace app\resource;
/**
 * 
 */
class Giftcard extends \app\models\Giftcard
{
	public function fields(){
        return ['id','email','balance','redemptionCode'];
    }

    public function extraFields(){
    	return ['created_at','updated_at'];
    }

}
<?php

return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=database;dbname=yii',
    'username' => 'root',
    'password' => 'tiger',
    'charset' => 'utf8',

    /*'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=xvcmmx20_yii',
    'username' => 'xvcmmx20_yiiuser',
    'password' => 'Da0tEnR=U[&O',
    'charset' => 'utf8',*/

    // Schema cache options (for production environment)
    //'enableSchemaCache' => true,
    //'schemaCacheDuration' => 60,
    //'schemaCache' => 'cache',
];

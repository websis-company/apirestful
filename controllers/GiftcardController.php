<?php 
namespace app\controllers;
use Yii;
use \app\resource\Giftcard;
use yii\data\ActiveDataProvider;
use yii\rest\ActiveController;
use yii\filters\auth\HttpBearerAuth;

/**
 * 
 */
class GiftcardController extends ActiveController
{
	public $modelClass = Giftcard::class;

    public function behaviors(){
        $behaviors = parent::behaviors();
        //$behaviors['authenticator']['only'] = ['create','update','delete']; //index & view are public
        $behaviors['authenticator']['authMethods'] = [
            HttpBearerAuth::class
        ];
        return $behaviors;
    }

    public function actions(){
        $actions = parent::actions();
        $actions['index']['prepareDataProvider'] = [$this,'prepareDataProvider'];

        return $actions;
    }


    public function prepareDataProvider(){
        $redemptionCode = \Yii::$app->request->get('redemptionCode');
        //$email          = \Yii::$app->request->get('email');
        /*$balance        = \Yii::$app->request->get('balance');*/

        return new ActiveDataProvider([
            'query' => $this->modelClass::find()->where(['redemptionCode'=> $redemptionCode])
            //'query' => $this->modelClass::find()->where(['redemptionCode'=> $redemptionCode,'email'=>$email])
            //'query' => $this->modelClass::find()->where(['email'=>$email])
        ]);
    }


}
<?php 
namespace app\controllers;


use \app\resource\Post;
use yii\filters\auth\HttpBearerAuth;
use yii\rest\ActiveController;

class PostController extends ActiveController{
	//public $modelClass = 'app\models\Post';
	public $modelClass = Post::class;

	//Authentication method
	public function behaviors(){
		$behaviors = parent::behaviors();
		$behaviors['authenticator']['only'] = ['create','update','delete']; //index & view are public
		$behaviors['authenticator']['authMethods'] = [
			HttpBearerAuth::class
		];
		return $behaviors;
	}
}


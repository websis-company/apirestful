<?php 
namespace app\controllers;


use \app\resource\Comment;
use yii\filters\auth\HttpBearerAuth;
use yii\data\ActiveDataProvider;
use yii\rest\ActiveController;
/**
 * 
 */
class CommentController extends ActiveController{
	public $modelClass = Comment::class;

	//Authentication method
	public function behaviors(){
		$behaviors = parent::behaviors();
		$behaviors['authenticator']['only'] = ['create','update','delete']; //index & view are public
		$behaviors['authenticator']['authMethods'] = [
			HttpBearerAuth::class
		];
		return $behaviors;
	}


	//search by filters [postId]
	public function actions(){
		$actions = parent::actions();
		$actions['index']['prepareDataProvider'] = [$this,'prepareDataProvider'];

		return $actions;
	}

	public function prepareDataProvider(){
		return new ActiveDataProvider([
			'query' => $this->modelClass::find()->andWhere(['post_id'=> \Yii::$app->request->get('postId')])
		]);
	}
}

